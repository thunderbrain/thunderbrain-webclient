export const config = {
	backendURI: process.env.BACKEND_URI || 'http://127.0.0.1:8001',
	backendWSURI: process.env.BACKEND_WSURI || 'ws://127.0.0.1:8001',
};
