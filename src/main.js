import Vue from 'vue';
import App from './App.vue';
import VueNativeSock from 'vue-native-websocket';

import { config } from './config.js';
import { EventBus } from './EventBus.js';

Vue.use(VueNativeSock, config.backendWSURI, {
	format: 'json',
	connectManually: true,
});
// Vue.config.productionTip = false;
new Vue({
	created: function () {
		this.$options.sockets.onmessage = this.wsReceive.bind(this);

		EventBus.$on('wsSend', data => {
			this.wsSend(data);
		});
		EventBus.$on('roomSelected', data => {
			this.wsConnect(data.roomURI);
		});
	},
	methods: {
		wsReceive: function (message) {
			let data = JSON.parse(message.data);
			console.log('wsReceive', data);

			if (data.event == 'roomSettingsChanged') {
				//odebranie ustawien pokoju z s2
				EventBus.$emit('roomSettingsChanged', data.payload);
			} else if (data.event == 'ideaAdded') {
				//odebranie nowego pomysłu z s2
				EventBus.$emit('ideaAdded', data.payload);
			} else if (data.event == 'commentAdded') {
				//dodano nowy pomysł
				EventBus.$emit('commentAdded', data.payload);
			} else if (data.event == 'userJoined') {
				//dołączył nowy user
				EventBus.$emit('userJoined', data.payload);
			} else if (data.event == 'userLeft') {
				//usuniecie uzytkownika z listy
				EventBus.$emit('userLeft', data.payload);
			} else if (data.event == 'connected') {
				let roomSettings = data.payload.roomSettings || {};
				let roomState = data.payload.roomState || {};
				let ideas = data.payload.ideas || [];
				let users = data.payload.users || [];
				switch (roomState.phase) {
				case 0: {
					//nie możemy się odwołać do jeszcze nie wyrenderowanych elementów
					Vue.nextTick(() => {
						EventBus.$emit('roomSettingsChanged', roomSettings);
						EventBus.$emit('roomUserList', users);
						EventBus.$emit('updateIdeasList', ideas);
					});
					break;
				}
				case 1: {
					EventBus.$emit('roomStart');
					Vue.nextTick(() => {
						EventBus.$emit('roomSettingsChanged', roomSettings);
						EventBus.$emit('roomUserList', users);
						EventBus.$emit('updateIdeasList', ideas);
					});
					break;
				}
				case 2: {
					EventBus.$emit('endIdeaPhase');
					Vue.nextTick(() => {
						EventBus.$emit('roomSettingsChanged', roomSettings);
						EventBus.$emit('roomUserList', users);
						EventBus.$emit('updateIdeasList', ideas);
					});
					break;
				}
				case 3: {
					//przyszliśmy na zakończone
					EventBus.$emit('end_brainstorm');
					Vue.nextTick(() => {
						EventBus.$emit('roomSettingsChanged', roomSettings);
						EventBus.$emit('roomUserList', users);
						EventBus.$emit('updateIdeasList', ideas);
					});
					break;
				}
				default: {
					console.log('invalid room state:', roomState);
				}
				}
			} else if (data.event == 'roomPhaseChanged') {
				if (data.payload.newPhase == 1) {
					//odebranie potwierdzenia zakonczenie f1 z s2
					EventBus.$emit('roomStart');
				} else if (data.payload.newPhase == 2) {
					//odebranie potwierdzenia zakonczenie f2 z s2
					EventBus.$emit('endIdeaPhase');
				} else if (data.payload.newPhase == 3) {
					//odebranie potwierdzenia zakonczenie f3 z s2
					EventBus.$emit('end_brainstorm');
				} else if (data.payload.newPhase == 4) {
					console.log('roomPhaseChanged newPhase == 4');
				}
			} else {
				console.log('unknown WS message received', data);
			}
		},
		wsSend: function (data) {
			return new Promise((resolve, reject) => {
				if (!this.$socket) {
					if (data.roomURI) {
						return this.wsConnect(data.roomURI).then(() => {
							resolve(this.$socket.sendObj(data));
						}).catch(reject);
					} else {
						return reject('no socket available!');
					}
				}
				return resolve(this.$socket.sendObj(data));
			});
		},
		wsConnect: function (roomURI) {
			return new Promise((resolve, reject) => {
				this.wsConnectRaw(roomURI, resolve, reject);
			});
		},
		wsConnectRaw: function (roomURI, resolve, reject) {
			let socketURI = config.backendWSURI + '/room/' + roomURI;
			if (this.$socket) {
				if (
					this.$socket.readyState === WebSocket.CLOSING ||
					this.$socket.readyState === WebSocket.CONNECTING
				) {
					console.log('WS connecting or disconnecting, wait...');
					setTimeout(this.wsConnectRaw.bind(this, roomURI, resolve, reject), 0.2);
					return;
				}

				if (this.$socket.readyState === WebSocket.OPEN) {
					if (this.$socket.url === socketURI) {
						console.log('WS already connected');
						resolve(this.$socket);
						return;
					} else {
						console.log('WS disconnecting old...');
						this.$disconnect();
						setTimeout(this.wsConnectRaw.bind(this, roomURI, resolve, reject), 0.2);
						return;
					}
				}
			}

			this.$connect(socketURI, { format: 'json' });
			resolve(this.$socket);
		},
	},
	render: h => h(App),
}).$mount('#app');
