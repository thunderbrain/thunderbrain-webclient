import getCookie from './getCookie';

export default getCookie.bind(getCookie, 'csrftoken');
