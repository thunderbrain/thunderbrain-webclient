# thunderbrain-webclient

## About
TBD

## Setup
No system-wise packages are required, everything happens within user's home directory.

### Prerequisites
1. Install NVM ([source of instructions](https://nvm.sh))
```sh
wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | $SHELL
```

Note: make sure that the lines the install script outputs at the end are within your shell's config file (`.bashrc` or similar).

Note2: re-source shell's configuration or open new terminal window to enable the changes before next step.

2. Install latest stable NodeJS using nvm
```sh
nvm install stable
```

### Dependencies
Regular:
```sh
npm install
```

## Running
Compiles and hot-reloads for development
```sh
npm run serve
```

Compiles and minifies for production
```sh
npm run build
```

Lint files
```sh
npm run lint
```

Lint and fix files
```sh
npm run lint_and_fix
```

Run tests
```sh
npm run test
```

Run unit tests
```sh
npm run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
