/// Simulates actions for login screen
import F1RoomSettingsView from '@/components/F1_RoomSettingsView.vue';
import RoomUserList from '@/components/lists/RoomUserList.vue';
import RoomSettingsForm from '@/components/RoomSettingsForm.vue';
import ForceEndPhase1Form from '@/components/ForceEndPhase1Form.vue';
import { mount } from '@vue/test-utils';
import { expect } from 'chai';
import { EventBus } from '@/EventBus.js';
import { axios, axiosresp } from './pseudoaxios';

describe('Room Settings screen', () => {
	const wrapper = mount(F1RoomSettingsView);

	it('shows forms on init', () => {
		expect(wrapper.contains(RoomUserList)).to.true;
		expect(wrapper.contains(RoomSettingsForm)).to.true;
		expect(wrapper.contains(ForceEndPhase1Form)).to.true;
	});

	it('fills users list', () => {
		const userList = wrapper.find(RoomUserList);
		//fill list
		EventBus.$emit('roomUserList', {
			roomURI: userList.vm.roomURI,
			users: [{
				username: 'xXx_pussydestroyer_xXx',
				first_name: 'Mścibór',
				last_name: 'Zabagienny'
			},
			{
				username: 'john69',
				first_name: 'Marianna',
				last_name: 'Przymorska'
			}]
		});
		//check filled
		expect(userList.text()).to.contain('xXx_pussydestroyer_xXx');
		expect(userList.text()).to.contain('Mścibór');
		expect(userList.text()).to.contain('Zabagienny');
		expect(userList.text()).to.contain('john69');
		expect(userList.text()).to.contain('Marianna');
		expect(userList.text()).to.contain('Przymorska');
	});

	it('adds user to list', () => {
		const userList = wrapper.find(RoomUserList);
		EventBus.$emit('roomUserAdded', {
			roomURI: userList.vm.roomURI,
			user: {
				username: 'anon',
				first_name: 'Anonimir',
				last_name: 'Anonimowitch'
			}
		});
		expect(userList.text()).to.contain('anon');
		expect(userList.text()).to.contain('Anonimir');
		expect(userList.text()).to.contain('Anonimowitch');
	});


	//TODO check checking register response

	it('hides forms when created', () => {
		EventBus.$emit('roomCreated');
		expect(wrapper.contains(RoomUserList)).to.false;
		expect(wrapper.contains(RoomSettingsForm)).to.false;
		expect(wrapper.contains(ForceEndPhase1Form)).to.false;
	});

});
