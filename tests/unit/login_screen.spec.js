/// Simulates actions for login screen
import E0LoginView from '@/components/E0_LoginView.vue';
import LoginForm from '@/components/LoginForm.vue';
import RegisterForm from '@/components/RegisterForm.vue';
import { mount } from '@vue/test-utils';
import { expect } from 'chai';
import { EventBus } from '@/EventBus.js';
import { axios, axiosresp } from './pseudoaxios';

describe('Login Screen', () => {
	const wrapper = mount(E0LoginView);

	it('shows forms on init', () => {
		expect(wrapper.contains(LoginForm)).to.true;
		expect(wrapper.contains(RegisterForm)).to.true;
	});
	it('shows forms when logged out', () => {
		EventBus.$emit('loggedOut');
		expect(wrapper.vm.showE0_LoginView).to.true;
		expect(wrapper.contains(LoginForm)).to.true;
		expect(wrapper.contains(RegisterForm)).to.true;
	});
	it('hides forms when logged in', () => {
		EventBus.$emit('loggedIn');
		expect(wrapper.vm.showE0_LoginView).to.false;
		expect(wrapper.contains(LoginForm)).to.false;
		expect(wrapper.contains(RegisterForm)).to.false;
	});
	it('reshows forms when logged out', () => {
		EventBus.$emit('loggedOut');
		expect(wrapper.vm.showE0_LoginView).to.true;
		expect(wrapper.contains(LoginForm)).to.true;
		expect(wrapper.contains(RegisterForm)).to.true;
	});



	it('sends login credentials', () => {
		const loginForm = wrapper.find(LoginForm);
		//put username
		expect(loginForm.findAll('input').at(0).attributes().name).to.equal('username');
		loginForm.findAll('input').at(0).setValue('dracula');
		//put password
		expect(loginForm.findAll('input').at(1).attributes().name).to.equal('password');
		loginForm.findAll('input').at(1).setValue('admin1');
		//send
		expect(loginForm.findAll('input').at(2).attributes().type).to.equal('submit');
		loginForm.findAll('input').at(2).trigger('submit');
		//check request
		expect(axiosresp.axiedData.username).to.equal('dracula');
		expect(axiosresp.axiedData.password).to.equal('admin1');
	});

	//TODO check checking login response

	it('sends register credentials', () => {
		const registerForm = wrapper.find(RegisterForm);
		const inputs = registerForm.findAll('input');
		//put username
		expect(inputs.at(0).attributes().name).to.equal('username');
		inputs.at(0).setValue('nosferatu');
		//put password
		expect(inputs.at(1).attributes().name).to.equal('password');
		inputs.at(1).setValue('admin2');
		expect(inputs.at(2).attributes().name).to.equal('password2');
		inputs.at(2).setValue('admin2');
		//put name
		expect(inputs.at(3).attributes().name).to.equal('firstName');
		inputs.at(3).setValue('Janusz');
		//put surname
		expect(inputs.at(4).attributes().name).to.equal('lastName');
		inputs.at(4).setValue('Korwin-Mikke');
		//put email
		expect(inputs.at(5).attributes().name).to.equal('email');
		inputs.at(5).setValue('krul@korwin.pl');
		//register
		expect(inputs.at(6).attributes().type).to.equal('submit');
		inputs.at(6).trigger('submit');
		//check request
		expect(axiosresp.axiedData.username).to.equal('nosferatu');
		expect(axiosresp.axiedData.password).to.equal('admin2');
		expect(axiosresp.axiedData.firstName).to.equal('Janusz');
		expect(axiosresp.axiedData.lastName).to.equal('Korwin-Mikke');
		expect(axiosresp.axiedData.email).to.equal('krul@korwin.pl');

	});

	//TODO check checking register response

});
