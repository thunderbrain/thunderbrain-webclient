/// Simulates actions for room selection and creation
import UserRoomsList from '@/components/UserRoomsList.vue';
import CreateRoomForm from '@/components/CreateRoomForm.vue';
import JoinRoom from '@/components/JoinRoom.vue';
import F0CreateRoomView from '@/components/F0_CreateRoomView.vue';
import JoinRoomURI from '@/components/JoinRoomURI.vue';
import { mount } from '@vue/test-utils';
import { expect } from 'chai';
// import { EventBus } from '@/EventBus.js';
import { axiosresp } from './pseudoaxios.js';

describe('Room creation', () => {
	const wrapper = mount(F0CreateRoomView);

	it('shows forms on init', () => {
		expect(wrapper.vm.showF0_CreateRoomView).to.true;
		expect(wrapper.contains(UserRoomsList)).to.true;
		expect(wrapper.contains(CreateRoomForm)).to.true;
	});

	it('fills user rooms list', () => {
		const roomsList = wrapper.find(UserRoomsList);
		roomsList.vm.rooms = [];
		expect(roomsList.find(JoinRoom).exists()).to.false;

		//force items into list
		roomsList.vm.rooms = [{roomURI: '1234', name: 'Sypialnia'}, {roomURI: '10', name: 'Garderoba'}];
		expect(roomsList.findAll(JoinRoom).length).to.equal(2);

		expect(roomsList.find(JoinRoom).text()).to.contain('Sypialnia');
		//click join putton
		expect(roomsList.find(JoinRoom).find('input').attributes().type).to.equal('submit');
		roomsList.find(JoinRoom).find('input').trigger('submit');
		//check request
		expect(axiosresp.axiedData.roomURI).to.equal('1234');

	});

	it('sends room creation request', () => {
		const creationForm = wrapper.find(CreateRoomForm);
		const inputs = creationForm.findAll('input');

		expect(inputs.at(0).attributes().name).to.equal('password');
		inputs.at(0).setValue('admin3');

		expect(inputs.at(1).attributes().name).to.equal('password2');
		inputs.at(1).setValue('admin3');

		expect(inputs.at(2).attributes().type).to.equal('submit');
		inputs.at(2).trigger('submit');

		expect(axiosresp.axiedData.password).to.equal('admin3');
	});

	it('sends join room via URI', () => {
		const uriForm = wrapper.find(JoinRoomURI);
		const inputs = uriForm.findAll('input');

		expect(inputs.at(0).attributes().name).to.equal('roomURI');
		inputs.at(0).setValue('https://twojastara.pl');

		expect(inputs.at(1).attributes().type).to.equal('submit');
		inputs.at(1).trigger('submit');

		expect(axiosresp.axiedData.roomURI).to.equal('https://twojastara.pl');
	});

	//TODO reactions for server response

});

